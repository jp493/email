import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
	contactEmail() { return faker.internet.email(); },
    contactName() { return faker.name.findName(); },
	contactPhone: '123-223-2345'
});
