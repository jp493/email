import Ember from 'ember';

export default Ember.Controller.extend({
	sortedContacts: function () {
		return this.get('model.details').sortBy('name:asc');
	}.property('model.details.@each.name')
});
