import Ember from 'ember';

export default Ember.Route.extend({
  model({groupId}) {
    return this.store.findRecord('group', groupId, {include: 'details'});
  }
});
