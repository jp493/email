import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  contactName: attr('string'),
  contactPhone: attr('string'),
  contactEmail: attr('string')
});
